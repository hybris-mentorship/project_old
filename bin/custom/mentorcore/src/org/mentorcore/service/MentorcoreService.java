/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.mentorcore.service;

public interface MentorcoreService
{
	String getHybrisLogoUrl(String logoCode);

	void createLogo(String logoCode);
}
