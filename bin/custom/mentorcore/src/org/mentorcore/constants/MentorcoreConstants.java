/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.mentorcore.constants;

/**
 * Global class for all Mentorcore constants. You can add global constants for your extension into this class.
 */
public final class MentorcoreConstants extends GeneratedMentorcoreConstants
{
	public static final String EXTENSIONNAME = "mentorcore";

	private MentorcoreConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "mentorcorePlatformLogo";
}
